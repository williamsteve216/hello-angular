import { Component } from '@angular/core';
import { ProductItemComponent } from '../product-item/product-item.component';
import { NgFor } from '@angular/common';
import { IProduct } from '../interfaces/product.interface';

@Component({
  selector: 'app-product-list',
  standalone: true,
  imports: [ProductItemComponent, NgFor],
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.scss'
})

export class ProductListComponent {
  

  products:IProduct[]=[
    {
      name:"product 1",
      id:"product1",
      description:"Cillum pariatur ea aliquip ipsum esse amet quis irure. Velit aliquip adipisicing veniam dolore consectetur duis amet id deserunt amet. Aliqua et nostrud anim ad aute laborum occaecat. Quis quis laboris occaecat non officia. Non Lorem amet adipisicing non cillum adipisicing."
    }
  ]
}
